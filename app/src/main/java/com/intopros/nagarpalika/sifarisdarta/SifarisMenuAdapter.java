package com.intopros.nagarpalika.sifarisdarta;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;
import com.intopros.nagarpalika.landing.model.MenuModel;
import com.intopros.nagarpalika.landing.viewholder.TileView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SifarisMenuAdapter extends AbstractAdapter<MenuModel> {

    public SifarisMenuAdapter(List<MenuModel> sifarisMenuList) {
        mItemList = sifarisMenuList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        return new TileView(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sub_tiles, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        TileView holder = (TileView) viewHolder;
        holder.setTitle(mItemList.get(position).getTitle());
        holder.setIcon(mItemList.get(position).getImage());
        holder.rView.setOnClickListener(v -> {

        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
