package com.intopros.nagarpalika.sifarisdarta;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.landing.model.MenuModel;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class SifarisMenuActivity extends AbstractActivity {

    @BindView(R.id.rv_sifaris)
    RecyclerView rv_sifaris;

    SifarisMenuAdapter mAdapter;

    @Override
    public int setLayout() {
        return R.layout.activity_sifaris_menu;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.static_recommendation), true);
        rv_sifaris.setLayoutManager(new GridLayoutManager(getActivityContext(), 2));
        List<MenuModel> sifarisMenuList = new ArrayList<>();
        sifarisMenuList.add(new MenuModel(R.drawable.ic_card, "नागरिकता ससिफारिश"));
        sifarisMenuList.add(new MenuModel(R.drawable.ic_contract, "घर/जग्गा/बाटो ससिफारिश"));
        sifarisMenuList.add(new MenuModel(R.drawable.ic_tax, "कर ससिफारिश"));
        sifarisMenuList.add(new MenuModel(R.drawable.ic_other, "अन्य ससिफारिश"));
        mAdapter = new SifarisMenuAdapter(sifarisMenuList);
        rv_sifaris.setAdapter(mAdapter);
    }
}
