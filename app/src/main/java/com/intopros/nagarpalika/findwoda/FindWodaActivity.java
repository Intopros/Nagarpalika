package com.intopros.nagarpalika.findwoda;

import android.app.Activity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class FindWodaActivity extends AbstractActivity implements OnMapReadyCallback, WodaAdapter.OnWodaSelectedListener {

    @BindView(R.id.rv_woda_list)
    RecyclerView rv_woda_list;

    WodaAdapter mAdapter;
    List<WodaModel> wodaList = new ArrayList<>();

    private GoogleMap mMap;

    @Override
    public int setLayout() {
        return R.layout.activity_find_woda;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.menu_find_woda), true);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        wodaList.add(new WodaModel("वडा न. १", 27.7116875, 85.3155917));
        wodaList.add(new WodaModel("वडा न. २", 27.7112612, 85.3172654));
        wodaList.add(new WodaModel("वडा न. ३", 27.7111625, 85.3189413));
        wodaList.add(new WodaModel("वडा न. ४", 27.7110547, 85.3210914));
        wodaList.add(new WodaModel("वडा न. ५", 27.7100541, 85.3221315));
        wodaList.add(new WodaModel("वडा न. ६", 27.709565, 85.3223034));
        wodaList.add(new WodaModel("वडा न. ७", 27.7071225, 85.3224294));
        wodaList.add(new WodaModel("वडा न. ८", 27.7014817, 85.3238419));
        wodaList.add(new WodaModel("वडा न. ९", 27.7007701, 85.3193625));
        wodaList.add(new WodaModel("वडा न. १०", 27.6937082, 85.3198652));
        rv_woda_list.setLayoutManager(new LinearLayoutManager(getActivityContext(), LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new WodaAdapter(wodaList).setListener(this);
        rv_woda_list.setAdapter(mAdapter);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng woda = new LatLng(27.6462614, 85.3601788);
        mMap.addMarker(new MarkerOptions().position(woda).title("Woda"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(woda, 16.0f));
    }

    @Override
    public void pinLocation(String wodaName, double lat, double lng) {
        LatLng woda = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(woda).title(wodaName));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(woda, 16.0f));
    }
}
