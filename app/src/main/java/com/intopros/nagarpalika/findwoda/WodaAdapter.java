package com.intopros.nagarpalika.findwoda;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class WodaAdapter extends AbstractAdapter<WodaModel> {

    int row_index = -1;

    OnWodaSelectedListener listener;

    public WodaAdapter setListener(OnWodaSelectedListener wodaSelectedListener) {
        listener = wodaSelectedListener;
        return this;
    }

    public interface OnWodaSelectedListener {
        void pinLocation(String wodaName, double lat, double lng);
    }

    public WodaAdapter(List<WodaModel> wodaList) {
        mItemList = wodaList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_woda, viewGroup, false);
        return new WodaView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        WodaView holder = (WodaView) viewHolder;
        WodaModel model = mItemList.get(position);
        holder.setItem(model);
        holder.rootView.setOnClickListener(view -> {
            row_index = holder.getAdapterPosition();
            notifyDataSetChanged();
        });
        if (row_index == holder.getAdapterPosition()) {
            holder.makeSelection(true);
            listener.pinLocation(model.getWodaName(),model.getLat(), model.getLng());
        } else
            holder.makeSelection(false);
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
