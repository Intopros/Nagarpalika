package com.intopros.nagarpalika.smartwallet;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class SmartWalletActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_smartwallet;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.static_wallet),true);
        hideShadow();
    }
}
