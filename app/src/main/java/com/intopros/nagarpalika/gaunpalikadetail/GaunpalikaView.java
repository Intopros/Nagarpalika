package com.intopros.nagarpalika.gaunpalikadetail;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GaunpalikaView extends RecyclerView.ViewHolder {

    View rootView;
    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.iv_dropdown)
    ImageView iv_dropdown;
    @BindView(R.id.tv_description)
    TextView tv_description;

    public GaunpalikaView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);
    }

    public void setItem(GaunpalikaObject item) {
        tv_title.setText(item.getTitle());
        tv_description.setText(item.getDescription());
    }
}