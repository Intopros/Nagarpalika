package com.intopros.nagarpalika.gaunpalikadetail;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class GaunPalikaActivity extends AbstractActivity {

    @BindView(R.id.rv_gaunpalika)
    RecyclerView rv_gaunpalika;

    GaunpalikaAdapter mAdapter;
    List<GaunpalikaObject> gaunpalikaList = new ArrayList<>();

    @Override
    public int setLayout() {
        return R.layout.activity_gaun_palika;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle(getString(R.string.menu_sodhpuch), true);
        gaunpalikaList.add(new GaunpalikaObject("नगरपालिका भनेको के हो?", "नगरपालिका भनेको यस्तो स्थान हो जहाँ मानिसहरू शान्ति र सद्भावले बास गर्छन्, यो गाउँको क्षेत्र हो।"));
        gaunpalikaList.add(new GaunpalikaObject("नगरपालिका भनेको के हो?", "नगरपालिका भनेको यस्तो स्थान हो जहाँ मानिसहरू शान्ति र सद्भावले बास गर्छन्, यो गाउँको क्षेत्र हो।"));
        gaunpalikaList.add(new GaunpalikaObject("नगरपालिका भनेको के हो?", "नगरपालिका भनेको यस्तो स्थान हो जहाँ मानिसहरू शान्ति र सद्भावले बास गर्छन्, यो गाउँको क्षेत्र हो।"));
        gaunpalikaList.add(new GaunpalikaObject("नगरपालिका भनेको के हो?", "नगरपालिका भनेको यस्तो स्थान हो जहाँ मानिसहरू शान्ति र सद्भावले बास गर्छन्, यो गाउँको क्षेत्र हो।"));
        gaunpalikaList.add(new GaunpalikaObject("नगरपालिका भनेको के हो?", "नगरपालिका भनेको यस्तो स्थान हो जहाँ मानिसहरू शान्ति र सद्भावले बास गर्छन्, यो गाउँको क्षेत्र हो।"));
        gaunpalikaList.add(new GaunpalikaObject("नगरपालिका भनेको के हो?", "नगरपालिका भनेको यस्तो स्थान हो जहाँ मानिसहरू शान्ति र सद्भावले बास गर्छन्, यो गाउँको क्षेत्र हो।"));
        rv_gaunpalika.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        mAdapter = new GaunpalikaAdapter(gaunpalikaList);
        rv_gaunpalika.setAdapter(mAdapter);
    }
}
