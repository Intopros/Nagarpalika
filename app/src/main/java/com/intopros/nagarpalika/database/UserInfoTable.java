package com.intopros.nagarpalika.database;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Unique;

@Entity(nameInDb = "USER_INFO_TABLE")
public class UserInfoTable {

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("photourl")
    @Expose
    private String photourl;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobileno")
    @Expose
    private String mobileno;
    @SerializedName("api_token")
    @Expose
    private String apiToken;
    @Unique
    @SerializedName("usercount")
    @Expose
    private Integer usercount;

    @Generated(hash = 840646191)
    public UserInfoTable(String userid, String name, String photourl, String email,
            String mobileno, String apiToken, Integer usercount) {
        this.userid = userid;
        this.name = name;
        this.photourl = photourl;
        this.email = email;
        this.mobileno = mobileno;
        this.apiToken = apiToken;
        this.usercount = usercount;
    }

    @Generated(hash = 1354492153)
    public UserInfoTable() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public Integer getUsercount() {
        return usercount;
    }

    public void setUsercount(Integer usercount) {
        this.usercount = usercount;
    }

    public String getUserid() {
        return this.userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }

    public String getPhotourl() {
        return this.photourl;
    }

}