package com.intopros.nagarpalika.utils;

public interface OnDialogButtonSelect {

    void onSuccess();
    void onCancel();
    void onSkip();
}
