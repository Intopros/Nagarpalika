package com.intopros.nagarpalika.ongoingprojects;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.nagarpalika.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OnGoingProjectView extends RecyclerView.ViewHolder {
    @BindView(R.id.projectTitle)
    TextView projectTitle;
    @BindView(R.id.ongoingProjectImage)
    ImageView ongoingProjectImage;
    @BindView(R.id.projectBudget)
    TextView projectBudget;
    @BindView(R.id.projectCompletionDate)
    TextView projectCompletionDate;

    public OnGoingProjectView(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setUpViews(OnGoingProjectModel object, Activity activity) {
        projectTitle.setText(object.getProjectName());
        projectBudget.setText(object.getBudget());
        projectCompletionDate.setText(object.getCompletion_date());
        Glide.with(activity.getApplicationContext())
                .load(object.getImage())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_image))
                .apply(RequestOptions.centerCropTransform())
                .into(ongoingProjectImage);
    }
}
