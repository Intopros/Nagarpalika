package com.intopros.nagarpalika.ongoingprojects.detailedongoingprojects;

import android.app.Activity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonObject;
import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.base.AppController;
import com.intopros.nagarpalika.ongoingprojects.OnGoingProjectModel;

import java.io.Serializable;

import butterknife.BindView;

public class OnGoingProjectDetailedActivity extends AbstractActivity implements Serializable {
    @BindView(R.id.detailedTitle)
    TextView detailedTitle;
    @BindView(R.id.detailImage)
    ImageView detailImage;
    @BindView(R.id.projectBudget)
    TextView projectBudget;
    @BindView(R.id.projectSpan)
    TextView projectSpan;
    @BindView(R.id.startingDate)
    TextView startingDate;
    @BindView(R.id.projectCompletionDate)
    TextView projectCompletionDate;
    @BindView(R.id.projectSupervisor)
    TextView projectSupervisor;
    @BindView(R.id.projectLocation)
    TextView projectLocation;
    @BindView(R.id.projectDescription)
    TextView projectDescription;

    @Override
    public int setLayout() {
        return R.layout.activity_ongoing_project_detail;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Project Details",true);
        hideShadow();
        OnGoingProjectModel data = (OnGoingProjectModel) getIntent().getSerializableExtra("project");
        Log.d("checker", "onActivityCreated: value = "+data.getProjectName());
        setUpViews(data);

    }

    private void setUpViews(OnGoingProjectModel data) {
        detailedTitle.setText(data.getProjectName());
        projectBudget.setText(data.getBudget());
        projectSpan.setText(data.getTimespan());
        startingDate.setText(data.getStarting_date());
        projectCompletionDate.setText(data.getCompletion_date());
        projectSupervisor.setText(data.getSupervisor());
        projectLocation.setText(data.getLocation());
        projectDescription.setText(data.getDescription());
        Glide.with(getActivityContext())
                .load(data.getImage())
                .apply(RequestOptions.centerCropTransform())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_image))
                .into(detailImage);
    }
}
