package com.intopros.nagarpalika.ongoingprojects;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;
import com.intopros.nagarpalika.ongoingprojects.detailedongoingprojects.OnGoingProjectDetailedActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class OnGoingProjectAdapter extends AbstractAdapter {
    public OnGoingProjectAdapter(List<OnGoingProjectModel> onGoingProjectList, Activity activity) {
        this.mItemList = onGoingProjectList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.row_ongoing_project_item, viewGroup, false);
        return new OnGoingProjectView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        OnGoingProjectView holder = (OnGoingProjectView) viewHolder;
        OnGoingProjectModel obj = (OnGoingProjectModel) mItemList.get(position);
        holder.setUpViews(obj, activity);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, OnGoingProjectDetailedActivity.class);

                intent.putExtra("project", obj);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
