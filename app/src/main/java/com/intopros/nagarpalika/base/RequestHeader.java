package com.intopros.nagarpalika.base;

import android.os.Build;

import com.intopros.nagarpalika.BuildConfig;
import com.intopros.nagarpalika.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.nagarpalika.base.baseutils.sharedpreferences.SharedValue;
import com.intopros.nagarpalika.database.SyncDatabase;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestHeader {

    public static Interceptor getHeader() {

        return new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request request;
                SyncDatabase syncdb = new SyncDatabase();
                String gcm = SharedPreferencesHelper.getSharedPreferences(AppController.getContext(), SharedValue.fcmToken, "");
//                Log.d("sdfff","==="+gcm);
//                String apitoken = "Bearer mimVT46dwyAKravDqj3JoOkvEBSpSIIeuaSQBaKXXbnrOvIt9cpeFct4ptfV";
                String apitoken = "";
                if (!syncdb.getApiToken().isEmpty())
                    apitoken = "Bearer " + syncdb.getApiToken();

//                Log.d("auth","==="+apitoken);
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Appversion", BuildConfig.VERSION_NAME)
                        .addHeader("Androidid", Build.ID)
                        .addHeader("Gcm", gcm)
                        .addHeader("Devicename", Build.MANUFACTURER + Build.MODEL)
                        .addHeader("Authorization", apitoken);
                request = requestBuilder.build();

                return chain.proceed(request);
            }
        };
    }
}
