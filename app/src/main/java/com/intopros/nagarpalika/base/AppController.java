package com.intopros.nagarpalika.base;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.facebook.stetho.Stetho;
import com.intopros.nagarpalika.BuildConfig;
import com.intopros.nagarpalika.base.baseutils.GlidemageLoadingService;
import com.intopros.nagarpalika.database.DaoMaster;
import com.intopros.nagarpalika.database.DaoSession;
import com.intopros.nagarpalika.database.DbOpenHelper;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import ss.com.bannerslider.Slider;


public class AppController extends Application {
    public static Activity mActivity;
    private static DaoSession daoSession;
    static Context mContext;

    public static AppController get(Activity activity) {
        mActivity = activity;
        return (AppController) activity.getApplication();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static AppController getApp(Application application) {
        return (AppController) application;
    }

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        Slider.init(new GlidemageLoadingService(this));

//        FacebookSdk.sdkInitialize(getApplicationContext());
        if (BuildConfig.DEBUG)
            Stetho.initializeWithDefaults(this);
        try {
            DbOpenHelper helper = new DbOpenHelper(this, "nagarpalika");
            Database db = helper.getWritableDb();
            daoSession = new DaoMaster(db).newSession();
        } catch (Exception e) {

        }
    }

    public static <T> QueryBuilder<T> getQuery(Class<T> noteClass) {
        return daoSession.queryBuilder(noteClass);
    }

    public static DaoSession getDaoSession() {
        return daoSession;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
