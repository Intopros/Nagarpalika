package com.intopros.nagarpalika.landing.model;

import java.util.List;

public class HomeModel {
    String type;
    String title;
    int titleId;
    String id;
    int color;
    int icon;
    List<MenuModel> menuList;

    public HomeModel(String type, int titleId) {
        this.type = type;
        this.titleId = titleId;
    }

    public HomeModel(String type, String title, List<MenuModel> menuList) {
        this.type = type;
        this.title = title;
        this.menuList = menuList;
    }

    public HomeModel(String type) {
        this.type = type;
        this.title = title;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public HomeModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<MenuModel> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuModel> menuList) {
        this.menuList = menuList;
    }

    public int getTitleId() {
        return titleId;
    }

    public void setTitleId(int titleId) {
        this.titleId = titleId;
    }
}
