package com.intopros.nagarpalika.landing.viewholder;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.landing.adapter.GridAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuView extends RecyclerView.ViewHolder {

    public View rView;

    @BindView(R.id.title)
    public TextView title;
    @BindView(R.id.h_recycler)
    public RecyclerView h_recycler;

    public GridAdapter hadapter;

    public MenuView(View view) {
        super(view);
        rView = view;
        ButterKnife.bind(this, rView);
    }

    public void setAdapter(GridAdapter adapter) {
        h_recycler.setLayoutManager(new GridLayoutManager(rView.getContext(), 3));
        hadapter = adapter;
        h_recycler.setAdapter(hadapter);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }
}
