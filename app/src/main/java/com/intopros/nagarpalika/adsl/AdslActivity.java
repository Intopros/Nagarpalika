package com.intopros.nagarpalika.adsl;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class AdslActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_adsl;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("ADSL",true);
    }
}
