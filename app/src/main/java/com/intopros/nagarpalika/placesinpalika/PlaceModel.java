package com.intopros.nagarpalika.placesinpalika;

import com.intopros.nagarpalika.findwoda.WodaModel;

import java.util.List;

public class PlaceModel {
    String title;
    String image;
    List<SinglePlaceModel> placeList;

    public PlaceModel(String title, String image, List<SinglePlaceModel> placeList) {
        this.title = title;
        this.image = image;
        this.placeList = placeList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<SinglePlaceModel> getPlaceList() {
        return placeList;
    }

    public void setPlaceList(List<SinglePlaceModel> placeList) {
        this.placeList = placeList;
    }
}
