package com.intopros.nagarpalika.placesinpalika;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PlacesAdapter extends AbstractAdapter<PlaceModel> {
    int row_index = -1;

    OnWodaSelectedListener listener;

    public PlacesAdapter(List<PlaceModel> placeList) {
        mItemList = placeList;
    }

    public PlacesAdapter setListener(OnWodaSelectedListener wodaSelectedListener) {
        listener = wodaSelectedListener;
        return this;
    }

    public interface OnWodaSelectedListener {
        void pinLocation(List<SinglePlaceModel> placeList);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_places, viewGroup, false);
        return new PlacesView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        PlacesView holder = (PlacesView) viewHolder;
        PlaceModel model = mItemList.get(position);
        holder.setItem(model.getTitle());
        holder.rootView.setOnClickListener(view -> {
            row_index = holder.getAdapterPosition();
            notifyDataSetChanged();
        });
        if (row_index == holder.getAdapterPosition()) {
            holder.makeSelection(true);
            listener.pinLocation(model.getPlaceList());
        } else
            holder.makeSelection(false);
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
