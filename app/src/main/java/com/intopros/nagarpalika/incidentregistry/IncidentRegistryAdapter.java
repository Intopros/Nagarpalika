package com.intopros.nagarpalika.incidentregistry;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;
import com.intopros.nagarpalika.incidentregistry.birth.BirthActivity;
import com.intopros.nagarpalika.incidentregistry.death.DeathActivity;
import com.intopros.nagarpalika.incidentregistry.divorce.DivorceActivity;
import com.intopros.nagarpalika.incidentregistry.marriage.MarriageActivity;
import com.intopros.nagarpalika.incidentregistry.migration.MigrationActivity;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class IncidentRegistryAdapter extends AbstractAdapter {
    public IncidentRegistryAdapter(List<IncidentRegistryModel> incidentList, Activity activity) {
        this.mItemList = incidentList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.row_incident_registry_items,viewGroup,false);
        return new IncidentRegistryView(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        IncidentRegistryView holder = (IncidentRegistryView) viewHolder;
        IncidentRegistryModel obj = (IncidentRegistryModel) mItemList.get(position);
        holder.setUpViews(obj.getTitle(),obj.getSubtitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (obj.getId() == "1"){
                    activity.startActivity(new Intent(activity, BirthActivity.class));
                } else if (obj.getId() == "2"){
                    activity.startActivity(new Intent(activity, DeathActivity.class));
                } else if (obj.getId() == "3") {
                    activity.startActivity(new Intent(activity, MigrationActivity.class));
                } else if(obj.getId() == "4"){
                    activity.startActivity(new Intent(activity, MarriageActivity.class));
                } else if (obj.getId() == "5"){
                    activity.startActivity(new Intent(activity, DivorceActivity.class));
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
