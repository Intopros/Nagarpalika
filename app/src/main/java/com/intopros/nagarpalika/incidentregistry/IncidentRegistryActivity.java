package com.intopros.nagarpalika.incidentregistry;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.utils.StaticValues;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;

public class IncidentRegistryActivity extends AbstractActivity {
    @BindView(R.id.incidentRegistryRecycler)
    RecyclerView recyclerView;
    IncidentRegistryAdapter adapter;
    List<IncidentRegistryModel> incidentList = new ArrayList<>();
    @Override
    public int setLayout() {
        return R.layout.activity_incident_registry;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Incident Registry",true);
        hideShadow();
        incidentList.add(new IncidentRegistryModel("1",getResources().getString(R.string.static_birth),getResources().getString(R.string.static_for_birth_registration)));
        incidentList.add(new IncidentRegistryModel("2",getResources().getString(R.string.static_death),getResources().getString(R.string.static_for_death_registration)));
        incidentList.add(new IncidentRegistryModel("3",getResources().getString(R.string.static_migration),getResources().getString(R.string.static_for_migration_registration)));
        incidentList.add(new IncidentRegistryModel("4",getResources().getString(R.string.static_marriage),getResources().getString(R.string.static_for_marriage_registration)));
        incidentList.add(new IncidentRegistryModel("5",getResources().getString(R.string.static_divorce),getResources().getString(R.string.static_for_divorce_registration)));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        adapter = new IncidentRegistryAdapter(incidentList,getActivityContext());
        recyclerView.setAdapter(adapter);
    }


}
