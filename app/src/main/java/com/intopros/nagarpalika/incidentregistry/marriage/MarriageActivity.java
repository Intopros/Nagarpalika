package com.intopros.nagarpalika.incidentregistry.marriage;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class MarriageActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_incident_registry_marriage;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Marriage Registration",true);
        hideShadow();
    }
}
