package com.intopros.nagarpalika.incidentregistry;

import android.view.View;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class IncidentRegistryView extends RecyclerView.ViewHolder {
    @BindView(R.id.incidentRegistryMainTopic)
    TextView topic;
    @BindView(R.id.incidentRegistrySubTopic)
    TextView subTopic;
    public IncidentRegistryView(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);;
    }
    public void setUpViews(String title, String subtitles){
        this.topic.setText(title);
        this.subTopic.setText(subtitles);
    }
}
