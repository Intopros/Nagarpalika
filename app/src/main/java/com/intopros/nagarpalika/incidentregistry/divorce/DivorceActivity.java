package com.intopros.nagarpalika.incidentregistry.divorce;

import android.app.Activity;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class DivorceActivity extends AbstractActivity {
    @Override
    public int setLayout() {
        return R.layout.activity_incident_registry_divorce;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("Divorce Registration",true);
        hideShadow();
    }
}
