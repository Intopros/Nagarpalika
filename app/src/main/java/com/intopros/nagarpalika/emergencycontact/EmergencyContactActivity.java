package com.intopros.nagarpalika.emergencycontact;

import android.app.Activity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class EmergencyContactActivity extends AbstractActivity {

    @BindView(R.id.rv_emergency_contact)
    RecyclerView rv_emergency_contact;

    EmergencyContactAdapter mAdapter;
    List<EmergencyContactModel> emergencyContactList = new ArrayList<>();

    @Override
    public int setLayout() {
        return R.layout.activity_emergency_contact;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("आपतकालिन सम्पर्क नम्बरहरू", true);
        emergencyContactList.add(new EmergencyContactModel("आपतकालिन अस्पताल","९८०१०८३७४६"));
        emergencyContactList.add(new EmergencyContactModel("फायर स्टेशन","९७५१९६८५३२"));
        emergencyContactList.add(new EmergencyContactModel("पुलिस स्टेशन","९८१३७६५४९८"));
        emergencyContactList.add(new EmergencyContactModel("विद्युत् प्राधिकरण","९८४३७५५४७३"));
        emergencyContactList.add(new EmergencyContactModel("खाने पानी संस्थान","९८४१७७५५८४"));
        rv_emergency_contact.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        mAdapter = new EmergencyContactAdapter(emergencyContactList);
        rv_emergency_contact.setAdapter(mAdapter);
    }
}
