package com.intopros.nagarpalika.emergencycontact;

import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intopros.nagarpalika.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class EmergencyContactView extends RecyclerView.ViewHolder {

    View rootView;

    @BindView(R.id.tv_title)
    TextView tv_title;
    @BindView(R.id.tv_number)
    TextView tv_number;
    @BindView(R.id.iv_call)
    ImageView iv_call;

    public EmergencyContactView(View view) {
        super(view);
        rootView = view;
        ButterKnife.bind(this, rootView);
    }

    public void setItem(final EmergencyContactModel item) {
        tv_title.setText(item.getTitle());
        tv_number.setText(item.getNumber());
        iv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + item.getNumber()));
                rootView.getContext().startActivity(callIntent);
            }
        });
    }
}
