package com.intopros.nagarpalika.aboutpalika;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.intopros.nagarpalika.R;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StaffView extends RecyclerView.ViewHolder {
    @BindView(R.id.userPhoto)
    ImageView userPhoto;
    @BindView(R.id.nameStaff)
    TextView nameStaff;
    @BindView(R.id.postStaff)
    TextView postStaff;
    public StaffView(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
    public void setUpViews(String name,String post, String images){
        nameStaff.setText(name);
        postStaff.setText(post);
        Glide.with(itemView.getContext())
                .load(images)
                .apply(RequestOptions.placeholderOf(R.drawable.ic_account).circleCrop())
                .into(userPhoto);
    }
}
