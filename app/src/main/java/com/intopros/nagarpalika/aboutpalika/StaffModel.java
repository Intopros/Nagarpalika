package com.intopros.nagarpalika.aboutpalika;

public class StaffModel {
    String id;
    String image;
    String name;
    String post;

    public StaffModel(String id, String image, String name, String post) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.post = post;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }
}
