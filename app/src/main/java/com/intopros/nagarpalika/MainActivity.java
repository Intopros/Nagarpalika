package com.intopros.nagarpalika;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.intopros.nagarpalika.aboutpalika.AboutStaffActivity;
import com.intopros.nagarpalika.base.AbstractActivity;
import com.intopros.nagarpalika.emergencycontact.EmergencyContactActivity;
import com.intopros.nagarpalika.findwoda.FindWodaActivity;
import com.intopros.nagarpalika.gaunpalikadetail.GaunPalikaActivity;
import com.intopros.nagarpalika.landing.adapter.HomeAdapter;
import com.intopros.nagarpalika.landing.model.HomeModel;
import com.intopros.nagarpalika.landing.model.MenuModel;
import com.intopros.nagarpalika.notification.NotificationActivity;
import com.intopros.nagarpalika.ongoingprojects.OnGoingProjectsActivity;
import com.intopros.nagarpalika.placesinpalika.PlacesActivity;
import com.intopros.nagarpalika.profile.ProfileActivity;
import com.intopros.nagarpalika.utils.EditTextDialog;
import com.intopros.nagarpalika.utils.NavDrawerRow;
import com.intopros.nagarpalika.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends AbstractActivity {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.nav_view)
    NavigationView nav_view;

    @BindView(R.id.homelist)
    RecyclerView homelist;

    HomeAdapter adapter;

    NavDrawerRow menu_home,
            menu_profile,
            menu_places,
            menu_gaunpalika,
            menu_find_woda,
            menu_emergency_contact,
            menu_about_palika,
            menu_feedback,
            menu_ongoing_projects;
    ArrayList<HomeModel> mlist = new ArrayList<>();

    @Override
    public int setLayout() {
        return R.layout.activity_home_container;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("नगारपालिका", false);
        setupNavigationView();
        homelist.setLayoutManager(new LinearLayoutManager(getActivityContext()));
        adapter = new HomeAdapter(getActivityContext(), mlist);
        homelist.setAdapter(adapter);
        loadData();
    }

    private void setupNavigationView() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer_layout, null,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();
        menu_feedback = findViewById(R.id.menu_feedback);
        menu_home = findViewById(R.id.menu_home);
        menu_profile = findViewById(R.id.menu_profile);
        menu_places = findViewById(R.id.menu_places);
        menu_gaunpalika = findViewById(R.id.menu_gaunpalika);
        menu_about_palika = findViewById(R.id.menu_about_palika);
        menu_emergency_contact = findViewById(R.id.menu_emergency_contact);
        menu_find_woda = findViewById(R.id.menu_find_woda);
        menu_ongoing_projects = findViewById(R.id.menu_ongoing_projects);

        menu_home.setupView(R.drawable.ic_home_black, getString(R.string.menu_home), getString(R.string.menu_home_detail));
        menu_profile.setupView(R.drawable.ic_account, getString(R.string.menu_profile), getString(R.string.menu_profile_detail));
        menu_places.setupView(R.drawable.ic_place_black_24dp, getString(R.string.menu_places), getString(R.string.menu_places_detail));
        menu_find_woda.setupView(R.drawable.ic_search, getString(R.string.menu_find_woda), getString(R.string.menu_find_woda_detail));
        menu_about_palika.setupView(R.drawable.ic_info_outline, getString(R.string.static_about_staffs), getString(R.string.menu_about_palika_detail));
        menu_gaunpalika.setupView(R.drawable.ic_information, getString(R.string.menu_sodhpuch), getString(R.string.menu_sodhpuch_detail));
        menu_emergency_contact.setupView(R.drawable.ic_phone, getString(R.string.menu_emergency_contact), getString(R.string.menu_emergency_contact_detail));
        menu_feedback.setupView(R.drawable.ic_feedback, getString(R.string.menu_feedback), getString(R.string.menu_feedback_detail));
        menu_ongoing_projects.setupView(R.drawable.ic_start_up, getString(R.string.static_ongoing_projects), getString(R.string.static_see_ongoing_projects));

    }

    private void loadData() {
        mlist.clear();
        mlist.add(new HomeModel("slider"));
        List<MenuModel> serviceMenuList = new ArrayList<>();
        serviceMenuList.add(new MenuModel(R.drawable.ic_write, getResources().getString(R.string.static_incident)));
        serviceMenuList.add(new MenuModel(R.drawable.ic_recommendation, getResources().getString(R.string.static_recommendation)));
        serviceMenuList.add(new MenuModel(R.drawable.ic_wallet_filled_money_tool, getResources().getString(R.string.static_wallet)));
        serviceMenuList.add(new MenuModel(R.drawable.ic_sketch, getResources().getString(R.string.static_map)));
        mlist.add(new HomeModel("stats"));
        mlist.add(new HomeModel("menu", getResources().getString(R.string.static_services), serviceMenuList));

        List<MenuModel> paymentMenuList = new ArrayList<>();
        paymentMenuList.add(new MenuModel(R.drawable.ic_electricity, getResources().getString(R.string.static_nea)));
        paymentMenuList.add(new MenuModel(R.drawable.ic_tap, getResources().getString(R.string.static_khanepani)));
        paymentMenuList.add(new MenuModel(R.drawable.ic_satellite_tv, getResources().getString(R.string.static_dishhome)));
        paymentMenuList.add(new MenuModel(R.drawable.ic_electric_tower, getResources().getString(R.string.static_adsl)));
        paymentMenuList.add(new MenuModel(R.drawable.ic_worldlink, "Worldlink"));
        paymentMenuList.add(new MenuModel(R.drawable.ic_smartphone, getResources().getString(R.string.static_topup)));
        paymentMenuList.add(new MenuModel(R.drawable.vianet, "Vianet"));
        mlist.add(new HomeModel("menu", getResources().getString(R.string.static_payment), paymentMenuList));
        mlist.add(new HomeModel("button", R.string.static_about_palika));
        mlist.add(new HomeModel("button", R.string.static_places));
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.iv_navigation)
    public void openNavigation() {
        drawer_layout.openDrawer(Gravity.START);
    }

    @OnClick(R.id.iv_notification)
    public void openNotification() {
        startActivity(new Intent(getActivityContext(), NotificationActivity.class));
    }

    @OnClick(R.id.menu_home)
    public void home() {
        drawer_layout.closeDrawer(Gravity.START);
    }

    @OnClick(R.id.menu_emergency_contact)
    public void menu_emergency_contact() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), EmergencyContactActivity.class));
    }

    @OnClick(R.id.menu_places)
    public void menu_places() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), PlacesActivity.class));
    }

    @OnClick(R.id.menu_profile)
    public void menuProfile() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), ProfileActivity.class));
    }

    @OnClick(R.id.menu_feedback)
    public void menu_feedback() {
        drawer_layout.closeDrawer(Gravity.START);

        final EditTextDialog input = new EditTextDialog(getActivityContext(), null);
        input.setTitle("हामीलाई प्रतिक्रिया दिनुहोस्");
        input.btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(input.getFeedback())) {
                    input.dialog.dismiss();
                    Utilities.Toaster(getActivityContext(), "प्रतिक्रियाका लागि धन्यवाद :)");
                } else
                    Utilities.Toaster(getActivityContext(), "कृपया केही प्रतिक्रिया लेख्नुहोस्");
            }
        });
        input.setHint("तपाईंको प्रतिक्रिया यहाँ लेख्नुहोस्");
        input.setOk("पठाउनुहोस्");
        input.setClose();
        input.show();
    }

    @OnClick(R.id.menu_ongoing_projects)
    public void onGoingProjects() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), OnGoingProjectsActivity.class));
    }

    @OnClick(R.id.menu_gaunpalika)
    public void menuGaunpalika() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), GaunPalikaActivity.class));
    }

    @OnClick(R.id.menu_about_palika)
    public void menuAboutPalika() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), AboutStaffActivity.class));
    }

    @OnClick(R.id.menu_emergency_contact)
    public void menuEmergencyContact() {
        drawer_layout.closeDrawer(Gravity.START);
    }

    @OnClick(R.id.menu_find_woda)
    public void menuFindWoda() {
        drawer_layout.closeDrawer(Gravity.START);
        startActivity(new Intent(getActivityContext(), FindWodaActivity.class));
    }

}
