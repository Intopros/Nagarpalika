package com.intopros.nagarpalika.notification;

import android.app.Activity;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractAdapter;
import com.intopros.nagarpalika.utils.LoadingHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class NotificationAdapter extends AbstractAdapter<NotificationObject> {

    public final static int TEXT = 1;
    public final static int IMAGE = 2;

    public NotificationAdapter(Activity activity, List<NotificationObject> mList) {
        this.activity = activity;
        this.mItemList = mList;
    }

    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case IMAGE:
                return new ImageNotificationView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification_image, viewGroup, false));
            case TEXT:
                return new NotificationView(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification, viewGroup, false));
            default:
                return new LoadingHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_loading, viewGroup, false));
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (mItemList.get(position).getImage() == null) {
            return TEXT;
        } else {
            return IMAGE;
        }
    }

    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder viewHolder, final int i) {
        super.onBindViewHolder(viewHolder, i);
        if (viewHolder instanceof NotificationView) {
           /* NotificationView holder = (NotificationView) viewHolder;
            holder.setItem(mItemList.get(i));
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });*/
        } else if (viewHolder instanceof ImageNotificationView) {
            ImageNotificationView holder = (ImageNotificationView) viewHolder;
//            holder.setItem(mItemList.get(i));
            holder.rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    activity.startActivity(new Intent(activity,NoticeDetailActivity.class));
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}
