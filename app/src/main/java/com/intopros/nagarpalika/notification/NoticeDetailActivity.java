package com.intopros.nagarpalika.notification;

import android.app.Activity;
import android.os.Bundle;

import com.intopros.nagarpalika.R;
import com.intopros.nagarpalika.base.AbstractActivity;

public class NoticeDetailActivity extends AbstractActivity {

    @Override
    public int setLayout() {
        return R.layout.activity_notice_detail;
    }

    @Override
    public Activity getActivityContext() {
        return this;
    }

    @Override
    public void onActivityCreated() {
        setPageTitle("सुचना विवरण", true);
    }
}
